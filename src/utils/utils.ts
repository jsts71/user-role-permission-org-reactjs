export const arrayRange = (stop: number, start: number = 1, step: number = 1) =>
    Array.from(
        { length: (stop - start) / step + 1 + ((stop - start) % step > 0 ? 1 : 0) },
        (value, index) => start + index * step
    );