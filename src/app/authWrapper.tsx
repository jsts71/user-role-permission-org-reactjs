'use client'

import { useCheckTokenMutation } from "@/features/auth/redux/api"
import { changeAuthorized } from "@/features/auth/redux/reducer"
import { useEffect } from "react"
import { useDispatch } from "react-redux"

export function AuthWrapper({ children }: { children: React.ReactNode }) {

    const [checkToken, result] = useCheckTokenMutation()
    const dispatch = useDispatch();

    useEffect(() => {
        checkToken({})
    }, [])

    useEffect(() => {
        if (result.status === "rejected") {
            if ((result.error as any)?.status == 401) dispatch(changeAuthorized(false))
        }

    }, [result.status])

    return (
        <>
            {children}
        </>
    )
}