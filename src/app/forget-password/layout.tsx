
export default function Layout({ children, }: { children: React.ReactNode }) {
    return (
        <>
            <div className="container mx-auto px-10 py-10">
                <div className="grid grid-cols-1 md:grid-cols-5" >
                    <div className="md:col-start-2 md:col-span-3 mt-10 mb-10">
                        {children}
                    </div>
                </div>
            </div>
        </>
    )
}

