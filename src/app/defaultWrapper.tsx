'use client'
import { ToastContainer } from 'react-toastify';
import { PrimeReactProvider, PrimeReactContext } from 'primereact/api';

export function DefaultWrapper({ children }: { children: React.ReactNode }) {



    return (
        <>
            <PrimeReactProvider>
                {children}
                <ToastContainer
                    position="top-center"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light" />
            </PrimeReactProvider>
        </>
    )
}