'use client'

import { useState } from "react"

import DefaultTabs from "@/components/tabs/default.tabs"
import { PermissionListAdminView, PermissionViews, tabs } from "@/views/admin/permission"

const Page = () => {

    const [view, setView] = useState<PermissionViews>(PermissionViews.PermissionLists)

    return (
        <>
            <div className='relative flex dark:bg-rose-700 bg-orange-600 h-12 rounded'>
                <div className="my-auto px-5">Permission</div>
                <div className="absolute right-5">
                    <button className="px-5 h-8 m-2 bg-rose-900 rounded">Create</button>
                </div>
            </div>
            

            <DefaultTabs view={view} setView={setView} tabs={tabs} />

            {view == PermissionViews.PermissionLists && <PermissionListAdminView />}

        </>
    )
}

export default Page