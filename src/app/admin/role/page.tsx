const Page = () => {
    return (
        <>
        <div className='relative flex dark:bg-rose-700 bg-orange-600 h-12 rounded'>
            <div className="my-auto px-5">Role</div>
            <div className="absolute right-5">
                <button className="px-5 h-8 m-2 bg-rose-900 rounded">Create</button>
            </div>
        </div>
        </>
    )
}

export default Page