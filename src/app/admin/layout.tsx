'use client'

import SideNav from "@/components/navbars/sidenav"
import { RootState } from "@/redux/store";
import { useSelector } from "react-redux";

export default function AdminLayout({ children, }: { children: React.ReactNode }) {

  const authorized = useSelector((state: RootState) => state.auth.authorized);

  return (authorized ?
    <>
      <div className="flex h-[calc(100vh-4rem)]">
        <div className="hidden md:block md:w-1/4 overflow-auto no-scrollbar bg-clip-border rounded-sm bg-white text-gray-700 p-4 shadow-xl shadow-blue-gray-900/5" >
          <SideNav />
        </div>
        <div className="md:w-3/4 overflow-auto no-scrollbar p-8">
          {children}
        </div>
      </div>
    </>
    : <div className="px-600">You are not authorized!</div>
  )

}
