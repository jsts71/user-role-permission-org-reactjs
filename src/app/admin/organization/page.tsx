'use client'

import { useState } from "react"
import { PaperClipIcon } from '@heroicons/react/20/solid'
import { AllOrgAnalyticsAdminView, OrgAnalyticsAdminView, OrgCodeAdminView, OrgDetailsAdminView, OrgListAdminView, OrgUpdateAdminView, OrgViews, tabs } from "@/views/admin/organization"
import DefaultTabs from "@/components/tabs/default.tabs"


const Page = () => {

    const [view, setView] = useState<OrgViews>(OrgViews.OrgAnalytics)

    return (
        <>

            <div className='relative flex dark:bg-rose-700 bg-orange-600 h-12 rounded mb-2'>
                <div className="my-auto px-5">Organization Name</div>
                <div className="absolute right-5">
                    <button className="px-5 h-8 m-2 bg-rose-900 rounded">Create</button>
                </div>
            </div>

            <DefaultTabs view={view} setView={setView} tabs={tabs} />

            <div className="mt-5">
                {view == OrgViews.AllOrgAnalytics && <AllOrgAnalyticsAdminView />}
                {view == OrgViews.OrgUpdate && <OrgUpdateAdminView />}
                {view == OrgViews.OrgList && <OrgListAdminView />}
                {view == OrgViews.OrgDetails && <OrgDetailsAdminView />}
                {view == OrgViews.OrgAnalytics && <OrgAnalyticsAdminView />}
                {view == OrgViews.OrgCode && <OrgCodeAdminView />}
            </div>
        </>
    )
}

export default Page