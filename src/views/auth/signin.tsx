

import { SignInFeature } from "@/features/auth";

interface SignInViewProps {

}

export default function SignInView(props: SignInViewProps) {
    return (
        <>
            <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
                <div className="sm:mx-auto sm:w-full sm:max-w-sm h-auto w-10">
                    <img
                        className="mx-auto h-10 w-10"
                        src="/images/admin.png"
                        alt="Your Company"
                    />
                    <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-100">
                        Sign in to your account
                    </h2>
                </div>
                <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                    <SignInFeature />

                    <p className="mt-10 text-center text-sm text-gray-500">
                        Not an admin panel member?{' '}<br />
                        <a href="/signup" className="font-semibold leading-6 text-indigo-600 hover:text-indigo-500">
                            Sign up for an organization or employment.
                        </a>
                    </p>
                </div>
            </div>
        </>
    )
}