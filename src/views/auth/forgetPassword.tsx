import { ForgetPasswordFeature } from "@/features/auth";

interface ForgetPasswordViewProps {

}

export default function ForgetPasswordView(props: ForgetPasswordViewProps) {
    return (
        <>
            <ForgetPasswordFeature />
        </>
    )
}