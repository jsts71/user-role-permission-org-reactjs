import CreateUserAdminView from "./create.user.admin.view";
import UpdateUserAdminView from "./update.user.admin.view";
import UserListAdminView from "./list.user.admin.view";

enum UserViews {
    AllOrgUserAnalytics,
    AllUserAnalytics,
    AllOrgUserLists,
    UserLists,
    UserUpdate,
    UserDetails,
}

const tabs = [
    {
        view: UserViews.AllOrgUserAnalytics,
        name: "AllOrgUserAnalytics"
    },
    {
        view: UserViews.AllUserAnalytics,
        name: "AllUserAnalytics"
    },
    {
        view: UserViews.AllOrgUserLists,
        name: "AllOrgUserLists"
    },
    {
        view: UserViews.UserUpdate,
        name: "UserUpdate"
    },
    {
        view: UserViews.UserDetails,
        name: "UserDetails"
    }
]

export {
    CreateUserAdminView,
    UpdateUserAdminView,
    UserListAdminView,
    UserViews,
    tabs
}