import AllOrgAnalyticsAdminView from "./analytics.all.org.admin.view";
import OrgUpdateAdminView from "./update.org.admin.view";
import OrgListAdminView from "./list.org.admin.view";
import OrgDetailsAdminView from "./details.org.admin.view";
import OrgAnalyticsAdminView from "./analytics.org.admin.view";
import OrgCodeAdminView from "./code.org.admin.view";

enum OrgViews {
    AllOrgAnalytics,
    OrgUpdate,
    OrgList,
    OrgDetails,
    OrgAnalytics,
    OrgCode,
}

const tabs = [
    {
        view: OrgViews.AllOrgAnalytics,
        name: "All Analytics"
    },
    {
        view: OrgViews.OrgAnalytics,
        name: "Analytics"
    },
    {
        view: OrgViews.OrgUpdate,
        name: "Update"
    },
    {
        view: OrgViews.OrgList,
        name: "List"
    },
    {
        view: OrgViews.OrgDetails,
        name: "Details"
    },
    {
        view: OrgViews.OrgCode,
        name: "Code"
    }
]

export {
    OrgViews,
    AllOrgAnalyticsAdminView,
    OrgUpdateAdminView,
    OrgListAdminView,
    OrgDetailsAdminView,
    OrgAnalyticsAdminView,
    OrgCodeAdminView,
    tabs
}