import CreateRoleAdminView from "./create.role.admin.view";
import UpdateRoleAdminView from "./update.role.admin.view";
import RoleListAdminView from "./list.role.admin.view";

enum RoleViews {
    AllOrgRoleAnalytics,
    AllRoleAnalytics,
    AllOrgRoleLists,
    RoleLists,
    RoleUpdate,
    RoleDetails,
}

const tabs = [
    {
        view: RoleViews.AllOrgRoleAnalytics,
        name: "AllOrgRoleAnalytics"
    },
    {
        view: RoleViews.AllRoleAnalytics,
        name: "AllRoleAnalytics"
    },
    {
        view: RoleViews.AllOrgRoleLists,
        name: "AllOrgRoleLists"
    },
    {
        view: RoleViews.RoleUpdate,
        name: "RoleUpdate"
    },
    {
        view: RoleViews.RoleDetails,
        name: "RoleDetails"
    }
]

export {
    CreateRoleAdminView,
    UpdateRoleAdminView,
    RoleListAdminView,
    RoleViews,
    tabs
}