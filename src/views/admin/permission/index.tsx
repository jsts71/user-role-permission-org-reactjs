import CreatePermissionAdminView from "./create.permission.admin.view";
import UpdatePermissionAdminView from "./update.permission.admin.view";
import PermissionListAdminView from "./list.permission.admin.view";

enum PermissionViews {
    AllOrgPermissionAnalytics,
    AllPermissionAnalytics,
    AllOrgPermissionLists,
    PermissionLists,
    PermissionUpdate,
    PermissionDetails,
}

const tabs = [
    {
        view: PermissionViews.AllOrgPermissionAnalytics,
        name: "AllOrgPermissionAnalytics"
    },
    {
        view: PermissionViews.AllPermissionAnalytics,
        name: "AllPermissionAnalytics"
    },
    {
        view: PermissionViews.AllOrgPermissionLists,
        name: "AllOrgPermissionLists"
    },
    {
        view: PermissionViews.PermissionLists,
        name: "PermissionLists"
    },
    {
        view: PermissionViews.PermissionUpdate,
        name: "PermissionUpdate"
    },
    {
        view: PermissionViews.PermissionDetails,
        name: "PermissionDetails"
    }
]

export {
    CreatePermissionAdminView,
    UpdatePermissionAdminView,
    PermissionListAdminView,
    PermissionViews,
    tabs
}