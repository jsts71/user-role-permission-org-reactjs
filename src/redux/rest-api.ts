'use client'

import { createApi } from '@reduxjs/toolkit/query/react'
import type { BaseQueryFn } from '@reduxjs/toolkit/query/react'
import { RootState } from './store'
import { fetchBaseQuery } from '@reduxjs/toolkit/query'
import type {
  FetchArgs,
  FetchBaseQueryError,
} from '@reduxjs/toolkit/query'
import { changeAuthorized, updateToken } from '@/features/auth/redux/reducer'

const BACKEND_URL: string = process.env.API_BASE_URL ?? ''

const baseQuery = fetchBaseQuery({
  baseUrl: BACKEND_URL,
  prepareHeaders: (headers, { getState }) => {
    const token = (getState() as RootState).auth.token
    if (token) {
      headers.set('authorization', `Bearer ${token.access_token}`)
    }
    return headers;
  }
},
)

const baseQueryWithReauth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {

  let result = await baseQuery(args, api, extraOptions)
  if (result.error && result.error.status === 401) {
    if ((result.error.data as any).message == "Token expired") {
      if ((args as any)?.url == '/auth/check/token') {
        const refreshResult = await baseQuery({ url: '/auth/re-auth', method: "POST" }, api, extraOptions)
        if (refreshResult.error) return result
        else api.dispatch(updateToken((refreshResult.data as any).data.token))
      }
      console.log(args)
      const refreshResult = await baseQuery({ url: '/auth/re-auth', method: "POST" }, api, extraOptions)
      if (refreshResult.error) {
        api.dispatch(updateToken({ access_token: "", refresh_token: "" }))
        api.dispatch(changeAuthorized(false))
        window.location.href = "/signin"
      }
      api.dispatch(updateToken((refreshResult.data as any).data.token))
      result = await baseQuery(args, api, extraOptions)
    }
  }
  return result
}

const rest_api = createApi({
  baseQuery: baseQueryWithReauth,
  tagTypes: ['Client', 'Contract', 'User', 'Visit'],
  endpoints: (build) => ({}),
})

export default rest_api