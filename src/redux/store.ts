import organizationApi from '@/features/admin/organization/redux/api';
import authApi from '@/features/auth/redux/api'
import authReducer from '@/features/auth/redux/reducer'
import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import rest_api from './rest-api';

const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = combineReducers({
  [rest_api.reducerPath]: organizationApi.reducer,
  auth: authReducer
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    }).concat(rest_api.middleware)
})

export const persistor = persistStore(store)

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch