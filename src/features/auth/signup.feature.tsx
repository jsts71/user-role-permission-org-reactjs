'use client'

import { SignUpView } from "@/views/auth"
import { useEffect, useState } from "react"
import { useSignUpHook } from "./hook/useSignUp.hook"
import { useFormik } from "formik"
import { toast } from "react-toastify"

interface SignUpFeatureProps {

}

export default function SignUpFeature(props: SignUpFeatureProps) {

    const [state, setState] = useState<'form' | 'otp' | 'extra'>('form')
    const [enabled, setEnabled] = useState(true)

    const { initialValues, onSubmit, validationSchema, sendOTP, checkOTP, validateOrgCode } = useSignUpHook({ setState })
    const formik = useFormik({ initialValues, onSubmit, validateOnChange: false, validationSchema })
    const { handleSubmit, setFieldValue, errors, values, validateForm, validateField } = formik

    console.log("enabled-", enabled)

    useEffect(() => {
        if ((Object.keys(errors).length < 2 && errors.hasOwnProperty("OTP"))||Object.keys(errors).length < 1 ) {
            setEnabled(true)
            toast.info("Form verified")
        } else setEnabled(false)
    }, [errors])


    console.log(errors)

    return (
        <>
            <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                <img
                    className="mx-auto h-10 w-auto"
                    src="/images/admin.png"
                    alt="Your Company"
                />
                <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-100">
                    Sign Up your account
                </h2>
            </div>

            {
                state == 'form' &&
                <>
                    <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-3">
                            <label htmlFor="first-name" className="block text-sm font-medium leading-6 text-gray-100">
                                First name
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(v) => { setFieldValue('firstName', v.target.value).finally(() => { validateField('firstName') }) }}
                                    value={values.firstName}
                                    type="text"
                                    name="first-name"
                                    id="first-name"
                                    autoComplete="off"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.firstName && errors.firstName}</span>
                            </div>
                        </div>

                        <div className="sm:col-span-3">
                            <label htmlFor="last-name" className="block text-sm font-medium leading-6 text-gray-100">
                                Last name
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(v) => { setFieldValue('lastName', v.target.value).finally(() => { validateField('lastName') }) }}
                                    value={values.lastName}
                                    type="text"
                                    name="last-name"
                                    id="last-name"
                                    autoComplete="off"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.lastName && errors.lastName}</span>
                            </div>
                        </div>
                    </div>
                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="user-name" className="block text-sm font-medium leading-6 text-gray-100">
                                User name
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(v) => { setFieldValue('userName', v.target.value).finally(() => { validateField('userName') }) }}
                                    value={values.userName}
                                    type="text"
                                    name="user-name"
                                    id="user-name"
                                    autoComplete="off"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.userName && errors.userName}</span>
                            </div>
                        </div>
                    </div>
                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-100">
                                Email
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(v) => { setFieldValue('email', v.target.value).finally(() => { validateField('email') }) }}
                                    value={values.email}
                                    type="text"
                                    name="email"
                                    id="email"
                                    autoComplete="off"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.email && errors.email}</span>
                            </div>
                        </div>
                    </div>
                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="phone" className="block text-sm font-medium leading-6 text-gray-100">
                                Phone Number
                            </label>
                            <div className="relative mt-2 rounded-md shadow-sm">
                                {/* <div className="absolute inset-y-0 left-0 flex items-center">
                                    <label htmlFor="phone" className="sr-only">
                                        Country
                                    </label>
                                    <select
                                        id="country"
                                        name="country"
                                        className="h-full rounded-md border-0 bg-transparent py-0 px-2.5 pl-2 pr-7 text-gray-500 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                    >
                                        <option>USD</option>
                                        <option>CAD</option>
                                        <option>EUR</option>
                                    </select>
                                </div> */}
                                <input
                                    onChange={(v) => { setFieldValue('phone', v.target.value).finally(() => { validateField('phone') }) }}
                                    value={values.phone}
                                    type="string"
                                    name="phone"
                                    id="phone"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5  pl-2 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                    placeholder="phone"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.phone && errors.phone}</span>
                            </div>
                        </div>
                    </div>

                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-3">
                            <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-100">
                                Password
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(v) => { setFieldValue('password', v.target.value).finally(() => { validateField('password'); validateField('confirmPassword') }) }}
                                    value={values.password}
                                    type="password"
                                    name="password"
                                    id="password"
                                    autoComplete="off"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.password && errors.password}</span>
                            </div>
                        </div>

                        <div className="sm:col-span-3">
                            <label htmlFor="confirmPassword" className="block text-sm font-medium leading-6 text-gray-100">
                                Confirm Password
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(v) => { setFieldValue('confirmPassword', v.target.value).finally(() => { validateField('password'); validateField('confirmPassword') }) }}
                                    value={values.confirmPassword}
                                    autoComplete="off"
                                    type="password"
                                    name="confirm-password"
                                    id="confirm-password"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5  text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.confirmPassword && errors.confirmPassword}</span>
                            </div>
                        </div>
                    </div>

                    <div className="mt-6">
                        <button
                            onClick={() => { setState('extra') }}
                            type="submit"
                            // disabled={errors?.firstName || errors?.lastName || errors?.userName || errors?.email || errors?.phone || errors?.password || errors?.confirmPassword ? true : false}
                            // disabled={errors?.firstName || errors?.lastName || errors?.userName || errors?.email || errors?.phone || errors?.password || errors?.confirmPassword ? true : false}
                            className="flex w-full justify-center rounded-md bg-green-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-green-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                        >
                            Next
                        </button>
                    </div>
                </>
            }
            {
                state == "extra" &&
                <>
                    <div className="mt-10 border-b border-gray-900/10 pb-5">
                        <h2 className="text-base text-xl font-bold leading-7 text-red-400">User meta</h2>
                        <p className="mt-1 text-sm leading-6 text-gray-200">
                            One Time Password and Organization option.
                        </p>
                        <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                            <div className="sm:col-span-3">
                                <div className="space-y-10">
                                    <fieldset>
                                        <legend className="text-xl font-semibold leading-6 text-red-300">One time password</legend>
                                        <div className="mt-6 space-y-6">
                                            <div className="relative flex gap-x-3">
                                                <div className="flex h-6 items-center">
                                                    <input
                                                        onChange={(v) => { setFieldValue('phoneForOTP', !values.phoneForOTP) }}
                                                        checked={!values.phoneForOTP}
                                                        id="email"
                                                        name="email"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                                    />
                                                </div>
                                                <div className="text-sm leading-6">
                                                    <label htmlFor="email" className="font-medium text-gray-100">
                                                        Through email
                                                    </label>
                                                    <p className="text-gray-500">Get notified through email you provide.</p>
                                                </div>
                                            </div>
                                            <div className="relative flex gap-x-3">
                                                <div className="flex h-6 items-center">
                                                    <input
                                                        onChange={(v) => { setFieldValue('phoneForOTP', !values.phoneForOTP) }}
                                                        checked={values.phoneForOTP}
                                                        id="phone"
                                                        name="phone"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                                    />
                                                </div>
                                                <div className="text-sm leading-6">
                                                    <label htmlFor="phone" className="font-medium text-gray-100">
                                                        Through phone
                                                    </label>
                                                    <p className="text-gray-500">Get notified through phone sms.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            {/* <div className="sm:col-span-3">
                                <div className="space-y-10">
                                    <fieldset>
                                        <legend className="text-xl font-semibold leading-6 text-red-300">Organization</legend>
                                        <div className="mt-6 space-y-6">
                                            <div className="relative flex gap-x-3">
                                                <div className="flex h-6 items-center">
                                                    <input
                                                        id="user-as-org"
                                                        name="user-as-org"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                                    />
                                                </div>
                                                <div className="text-sm leading-6">
                                                    <label htmlFor="user-as-org" className="font-medium text-gray-100">
                                                        User as Organization
                                                    </label>
                                                    <p className="text-gray-500">Sign up as an organization.</p>
                                                </div>
                                            </div>
                                            <div className="relative flex gap-x-3">
                                                <div className="flex h-6 items-center">
                                                    <input
                                                        id="user-as-org"
                                                        name="user-as-org"
                                                        type="checkbox"
                                                        className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                                                    />
                                                </div>
                                                <div className="text-sm leading-6">
                                                    <label htmlFor="user-as-org" className="font-medium text-gray-100">
                                                        User as Employee
                                                    </label>
                                                    <p className="text-gray-500">Sign up as an employee.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div> */}
                        </div>
                    </div>

                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-100">
                                Organization Code
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(e) => { setFieldValue('organizationCode', e.target.value).finally(() => { validateField('organizationCode') }) }}
                                    value={values.organizationCode}
                                    type="text"
                                    name="organization-code"
                                    id="organization-code"
                                    autoComplete="off"
                                    className="block w-full px-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.organizationCode && errors.organizationCode}</span>
                            </div>
                        </div>
                    </div>

                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => { setState('form') }}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-orange-400 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-orange-300 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Prev
                                </button>
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    // onClick={() => { validateForm().then((result) => { console.log(Object.keys(result).length < 2 && result.hasOwnProperty("OTP"));Object.keys(result).length < 2 && result.hasOwnProperty("OTP") ? setEnabled(true) : setEnabled(false) }) }}
                                    onClick={() => { validateForm() }}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-green-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-green-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Validate
                                </button>
                            </div>
                        </div>
                        {/* sendOTP(values.phoneForOTP ? values.phone : values.email, values.organizationCode);  */}
                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => { validateOrgCode(values.phoneForOTP ? values.phone : values.email, values.organizationCode) }}
                                    disabled={!enabled}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-green-500 px-3 py-1.5  text-sm font-semibold leading-6 text-white shadow-sm hover:bg-green-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 disabled:bg-gray-500"
                                >
                                    Next
                                </button>
                            </div>
                        </div>
                    </div>
                </>
            }
            {
                state == 'otp' &&
                <>
                    <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="otp" className="block text-sm font-medium leading-6 text-gray-100">
                                OTP
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(e) => { setFieldValue('OTP', e.target.value).finally(() => { validateField('OTP'); }) }}
                                    value={values.OTP}
                                    type="number"
                                    name="otp"
                                    id="otp"
                                    autoComplete="off"
                                    className="block w-full px-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <span className="text-sm mt-10" style={{ color: "red" }}>{errors.OTP && errors.OTP}</span>
                            </div>
                        </div>
                    </div>
                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => { setState('form') }}
                                    className="flex w-full justify-center rounded-md bg-red-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-red-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Change Info
                                </button>
                            </div>
                        </div>

                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => { sendOTP(values.phoneForOTP ? values.phone : values.email); }}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-orange-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-orange-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Resend
                                </button>
                            </div>
                        </div>

                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => handleSubmit()}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-green-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-green-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </>

            }
        </>
    )
}