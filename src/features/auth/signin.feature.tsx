'use client'
import { useAuthHook } from "./hook/useAuth.hook"
import { useFormik } from 'formik'

interface SignInFeatureProps {

}

export default function SignInFeature(props: SignInFeatureProps) {

    const { initialValues, onSubmit, validationSchema } = useAuthHook({})
    const formik = useFormik({ initialValues, onSubmit, validateOnChange: true, validationSchema })
    const { handleSubmit, setFieldValue, errors, values } = formik

    return (
        <>
            <form className="space-y-6" action="#" method="POST">
                <div>
                    <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-100">
                        Email address
                    </label>
                    <div className="mt-2">
                        <input
                            onChange={(v) => { setFieldValue('email', v.target.value) }}
                            value={values.email}
                            id="email"
                            name="email"
                            type="email"
                            autoComplete="email"
                            required
                            className="block w-full rounded-md border-0 py-1.5 p-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        />
                        <span className="text-sm mt-10 text-gray-400" style={{ color: "red" }}>{errors.email && errors.email}</span>

                    </div>
                </div>

                <div>
                    <div className="flex items-center justify-between">
                        <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-100">
                            Password
                        </label>
                        <div className="text-sm">
                            <a href="/forget-password" className="font-semibold text-indigo-600 hover:text-indigo-500">
                                Forgot password?
                            </a>
                        </div>
                    </div>
                    <div className="mt-2">
                        <input
                            onChange={(v) => { setFieldValue('password', v.target.value) }}
                            value={values.password}
                            id="password"
                            name="password"
                            type="password"
                            autoComplete="current-password"
                            required
                            className="block w-full rounded-md border-0 py-1.5 p-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                        />
                        <span className="text-sm mt-10" style={{ color: "red" }}>{errors.password && errors.password}</span>
                    </div>
                </div>

                <div className="relative flex gap-x-3">
                    <div className="flex h-6 items-center">
                        <input
                            id="offers"
                            name="offers"
                            type="checkbox"
                            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                            checked={values.rememberMe}
                            onChange={(v) => { setFieldValue('rememberMe', !values.rememberMe) }}
                        />
                    </div>
                    <div className="text-sm leading-6">
                        <label htmlFor="offers" className="font-medium text-gray-200">
                            Remember me
                        </label>
                    </div>
                </div>

                <div>
                    <button
                        type="button" style={{ background: "red" }}
                        className="flex w-full justify-center rounded-md dark:bg-indigo-600 bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                        onClick={() => handleSubmit()}
                    >
                        Sign in
                    </button>
                </div>
            </form>
        </>
    )
}