'use client'

import { useCallback, useEffect, useState } from "react"
import { useLoginMutation } from "../redux/api"
import { LoginDTO, authValidationSchema } from "../dto/auth.dto";
import { ObjectSchema } from "yup";
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from "@/redux/store";
import { changeAuthorized, updateToken } from "../redux/reducer";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

export interface AuthHookProps {

}

export interface AuthHookInterface {
    initialValues: LoginDTO
    onSubmit: (values: any) => void
    validationSchema: ObjectSchema<LoginDTO>
}

export const useAuthHook = (props: AuthHookProps): AuthHookInterface => {

    const token = useSelector((state: RootState) => state.auth.token);
    const dispatch = useDispatch();
    const router = useRouter()
    const [initialValues, setInitialValues] = useState({ phone: '', email: '', userName: '', password: '', rememberMe: false });
    const [validationSchema, setValidationSchema] = useState(authValidationSchema);


    const [login, loginResult] = useLoginMutation();

    useEffect(() => {
        if (!loginResult.isLoading && loginResult.isSuccess) {
            dispatch(updateToken(loginResult.data.data))
            if (loginResult.data.data.access_token) {
                dispatch(changeAuthorized(true))
                router.push('/')
            }
        }
    }, [loginResult.data])

    async function onSubmit(values: any) {
        console.log(values)
        login(values).then((result: any) => {
            console.log(result)
            if (result?.data?.success) {
                toast.success(result.data.message)
            } else {
                toast.error(result.error.data.message)
            }
        }).catch((error: any) => {
            console.log(error)
            toast.error("Something went wrong!")
        })
    }

    return {
        initialValues,
        onSubmit,
        validationSchema,
    }
}