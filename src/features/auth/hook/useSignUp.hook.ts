'use client'

import { useCallback, useEffect, useState } from "react"
import { useCheckContactOTPMutation, useCheckOrgCodeMutation, useLoginMutation, useSignUpMutation, useSignWithContactMutation } from "../redux/api"
import { LoginDTO, SignUpDTO, authValidationSchema, signUpInitialValue, signUpValidationSchema } from "../dto/auth.dto";
import { ObjectSchema } from "yup";
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from "@/redux/store";
import { changeAuthorized, updateToken } from "../redux/reducer";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

export interface SignUpHookProps {
    setState: (state: 'form' | 'otp' | 'extra') => void
}

export interface SignUpHookInterface {
    initialValues: SignUpDTO
    onSubmit: (values: any) => void
    validationSchema: ObjectSchema<SignUpDTO>
    sendOTP: (contact: string, code?: string) => void
    checkOTP: (contact: string, OTP: string) => void
    validateOrgCode: (contact: string, code: string) => any
}

export const useSignUpHook = (props: SignUpHookProps): SignUpHookInterface => {

    const { setState } = props

    const token = useSelector((state: RootState) => state.auth.token);
    const dispatch = useDispatch();
    const router = useRouter()

    const [initialValues, setInitialValues] = useState(signUpInitialValue);
    const [validationSchema, setValidationSchema] = useState(signUpValidationSchema);
    const [signUp, signUpResult] = useSignUpMutation();
    const [checkContactOTP, checkContactOTPResult] = useCheckContactOTPMutation();
    const [signWithContact, signWithContactResult] = useSignWithContactMutation();
    const [checkOrgCode, checkOrgCodeResult] = useCheckOrgCodeMutation();

    useEffect(() => {

    }, [])

    const onSubmit = (values: SignUpDTO) => {
        console.log(values)
        signUp(values).then((result: any) => {
            console.log(result)
            if (result?.data?.success) {
                toast.success(result.data.message)
                router.push('signin')
            } else {
                toast.error(result.error.data.message)
            }
        }).catch((error: any) => {
            console.log(error)
            toast.error("Something went wrong!")
        })
    }


    const sendOTP = (contact: string, code?: string) => {
        if (code) {
            checkOrgCode({ code }).then((result: any) => {
                console.log(result)

                if (result?.data?.success) {
                    toast.success(result.data.message)

                    signWithContact({ contact }).then((result2: any) => {
                        if (result2.data.success) {
                            toast.success(result2.data.message)
                            setState('otp')
                        } else {
                            toast.error(result2.data.message)
                        }
                    })
                } else {
                    toast.error(result.error.data.message)
                }
            })
        } else {
            signWithContact({ contact }).then((result: any) => {
                if (result.data.success) {
                    toast.success(result.data.message)
                    setState('otp')
                } else {
                    toast.error(result.data.message)
                }
            })
        }
    }

    const checkOTP = (contact: string, OTP: string) => {
        checkContactOTP({ contact, OTP }).then((result: any) => {
            console.log(result)
            if (result?.data?.success) {
                toast.success(result.data.message)
            } else {
                toast.error(result.error.data.message)
            }
        }).catch((error: any) => {
            console.log(error)
            toast.error("Something went wrong!")
        })
    }

    const validateOrgCode = (contact: string, code: string) => {
        checkOrgCode({ code }).then((result: any) => {
            console.log(result)
            if (result?.data?.success) {
                
                signWithContact({ contact }).then((result2: any) => {
                    if (result2.data.success) {
                        toast.success(result2.data.message)
                    } else {
                        toast.error(result2.data.message)
                    }
                })

                toast.success(result.data.message)
                setState("otp")

            } else {
                toast.error(result.error.data.message)
            }
        }).catch((error: any) => {
            console.log(error)
            toast.error("Something went wrong!")
        })
    }


    return {
        initialValues,
        onSubmit,
        validationSchema,
        sendOTP,
        checkOTP,
        validateOrgCode
    }
}