'use client'

import { EyeIcon, EyeSlashIcon, XMarkIcon } from "@heroicons/react/20/solid"
import { useState } from "react"
import { useCheckContactOTPMutation, useSignWithContactMutation, useUpdatePasswordByOTPMutation } from "./redux/api";
import { toast } from 'react-toastify';
import { useRouter } from 'next/navigation'

interface ForgetPasswordFeatureProps {

}

export default function ForgetPasswordFeature(props: ForgetPasswordFeatureProps) {

    const [contact, setContact] = useState('')
    const [OTP, setOTP] = useState('')
    const [password, setPassword] = useState('')
    const router = useRouter()

    const [checkContactOTP, checkContactOTPResult] = useCheckContactOTPMutation();
    const [updatePasswordByOTP, updatePasswordByOTPResult] = useUpdatePasswordByOTPMutation();
    const [signWithContact, signWithContactResult] = useSignWithContactMutation();

    const [state, setState] = useState<'contact' | 'check_otp' | 'password'>('contact')
    const [hidden, setHidden] = useState(true)
    const [counter, setCounter] = useState(100)

    const sendOTP = () => {
        signWithContact({ contact }).then((result: any) => {
            if (result.data.success) {
                toast.success(result.data.message)
                setState('check_otp')
                setCounter(60 * 2)
            } else {
                toast.error(result.data.message)
            }
        })
    }

    const checkOTP = () => {
        checkContactOTP({ contact, OTP }).then((result: any) => {
            console.log(result)
            if (result?.data?.success) {
                toast.success(result.data.message)
                setState('password')
                setCounter(60 * 5)
            } else {
                toast.error(result.error.data.message)
            }
        }).catch((error: any) => {
            console.log(error)
            toast.error("Something went wrong!")
        })
    }

    const changePassword = () => {
        updatePasswordByOTP({ contact, password, OTP }).then((result: any) => {
            if (result?.data?.success) {
                router.push('/signin', { scroll: false })
                toast.success(result.data.message)
            } else {
                toast.error(result.error.data.message)
            }
        }).catch((error) => {
            console.log(error)
            toast.error("Something went wrong!")
        })
        // router.push('/sign-in', { scroll: false })
    }

    return (
        <>
            <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                <img
                    className="mx-auto h-10 w-auto"
                    src="/images/admin.png"
                    alt="Your Company"
                />
                <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-100">
                    Forgot Password
                </h2>
            </div>

            {
                state == 'contact' &&
                <>
                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="phone" className="block text-sm font-medium leading-6 text-gray-100">
                                Contact
                            </label>
                            <div className="relative mt-2 rounded-md shadow-sm">
                                <input
                                    onChange={(e) => setContact(e.target.value)}
                                    value={contact}
                                    type="text"
                                    name="contact"
                                    id="contact"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5  pl-2 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                    placeholder="contact"
                                />
                            </div>
                        </div>
                    </div>


                    <div className="mt-6">
                        <button
                            onClick={() => sendOTP()}
                            type="submit"
                            className="flex w-full justify-center rounded-md bg-green-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-green-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                        >
                            Send OTP
                        </button>
                    </div>
                </>
            }

            {
                state == 'check_otp' &&
                <>
                    <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="otp" className="block text-sm font-medium leading-6 text-gray-100">
                                OTP
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(e) => setOTP(e.target.value)}
                                    value={OTP}
                                    type="text"
                                    name="otp"
                                    id="otp"
                                    autoComplete="family-name"
                                    className="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>
                    </div>


                    <div className="relative flex gap-x-3">
                        <div className="text-sm leading-6">
                            <label htmlFor="offers" className="font-medium text-gray-200">
                                {counter + " s"}
                            </label>
                        </div>
                    </div>

                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => { setState('contact') }}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-gray-500 px-1 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-red-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Change Contact
                                </button>
                            </div>
                        </div>

                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => sendOTP()}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-red-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-red-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Resend OTP
                                </button>
                            </div>
                        </div>

                        <div className="sm:col-span-2">
                            <div className="mt-3">
                                <button
                                    onClick={() => checkOTP()}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-green-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-green-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Check OTP
                                </button>
                            </div>
                        </div>
                    </div>
                </>
            }
            {
                state == 'password' &&
                <>
                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-100">
                                Password
                            </label>
                            <div className="relative mt-2">
                                <input
                                    onChange={(e) => setPassword(e.target.value)}
                                    value={password}
                                    type={`${hidden ? 'password' : 'text'}`}
                                    name="password"
                                    id="password"
                                    autoComplete="given-name"
                                    className="block w-full rounded-md border-0 py-1.5 px-2.5 pr-20 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                                <div className="absolute inset-y-0 right-0 flex items-center rounded-md">
                                    {
                                        hidden ?
                                            <EyeIcon title="Remove" onClick={() => { setHidden(false) }} className="h-9 bg-lime-600 px-3 rounded-md" /> :
                                            <EyeSlashIcon title="Remove" onClick={() => { setHidden(true) }} className="h-9 bg-rose-600 px-3 rounded-md" />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="relative flex gap-x-3">
                        <div className="text-sm leading-6">
                            <label htmlFor="offers" className="font-medium text-gray-200">
                                {counter + " s"}
                            </label>
                        </div>
                    </div>

                    <div className="mt-3 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">

                        <div className="sm:col-span-6">
                            <div className="mt-3">
                                <button
                                    onClick={() => changePassword()}
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-green-500 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-green-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </>
            }
        </>
    )
}