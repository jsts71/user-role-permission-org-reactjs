'use client'
import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

interface token {
    access_token: string,
    refresh_token: string,
}

export interface AuthState {
    token: token,
    authorized: boolean,
}

const initialState: AuthState = {
    token: {
        access_token: '',
        refresh_token: '',
    },
    authorized: false
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        updateToken: (state, action: PayloadAction<token>) => {
            state.token = action.payload
        },
        removeToken: (state) => {
            state.token = initialState.token
        },
        changeAuthorized: (state, action: PayloadAction<boolean>) => {
            state.authorized = action.payload
        }
    },
})

export const {
    updateToken,
    removeToken,
    changeAuthorized
} = authSlice.actions

export default authSlice.reducer