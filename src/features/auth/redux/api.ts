
import rest_api from "@/redux/rest-api";
import { LoginDTO, SignUpDTO } from "../dto/auth.dto";


const authApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        signup: build.mutation({
            query: (payload: any) => ({ method: "", url: "", body: "" })
        }),
        checkToken: build.mutation({
            query: () => ({ method: "POST", url: "/auth/check/token" })
        }),
        login: build.mutation({
            query: (payload: LoginDTO) => ({ method: "POST", url: "/auth/login", body: payload })
        }),
        fetchAuthList: build.query({
            query: () => ({ method: "GET", url: "/auth" })
        }),
        checkContactOTP: build.mutation({
            query: (body: { contact: string, OTP: string }) => ({ method: "POST", url: "/auth/check/otp", body })
        }),
        updatePasswordByOTP: build.mutation({
            query: (body: { contact: string, OTP: string, password: string }) => ({ method: "POST", url: "/auth/update-password/by-otp", body })
        }),
        signWithContact: build.mutation({
            query: (body: { contact: string }) => ({ method: "POST", url: "/auth/sign-with-contact", body })
        }),
        signUp: build.mutation({
            query: (body: SignUpDTO) => ({ method: "POST", url: "/auth/signup", body })
        }),
        checkOrgCode: build.mutation({
            query: (body: { code: string }) => ({ method: "POST", url: "/auth/check/org-code", body })
        }),
    })
})


export const {
    useCheckTokenMutation,
    useLoginMutation,
    useSignupMutation,
    useFetchAuthListQuery,
    useCheckContactOTPMutation,
    useUpdatePasswordByOTPMutation,
    useSignWithContactMutation,
    useSignUpMutation,
    useCheckOrgCodeMutation
} = authApi;
export default authApi;