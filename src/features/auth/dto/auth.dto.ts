import { ObjectSchema, StringSchema, boolean, object, string, ref } from "yup";
import * as Yup from 'yup';

export class LoginDTO {
    email?: string
    phone?: string
    userName?: string
    password: string = ''
    rememberMe?: boolean = false

}
export class SignUpDTO {
    firstName: string = ''
    lastName: string = ''
    email: string = ''
    phone: string = ''
    userName: string = ''
    password: string = ''
    confirmPassword: string = ''
    organizationCode: string = ''
    OTP: string = ''
    phoneForOTP: boolean = false

}
export const signUpInitialValue = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    userName: '',
    password: '',
    confirmPassword: '',
    organizationCode: '',
    OTP: '',
    phoneForOTP: false

}

export const authValidationSchema: ObjectSchema<LoginDTO> = object({
    email: string(),
    phone: string(),
    userName: string(),
    password: string().required(),
    rememberMe: boolean(),
});

const phoneRegExp = /(^(\+88|0088)?(01){1}[3456789]{1}(\d){8})$/
export const signUpValidationSchema: ObjectSchema<SignUpDTO> = object({
    firstName: string().required(),
    lastName: string().required(),
    email: string().required().email(),
    phone: string().required().matches(phoneRegExp, 'Phone number is not valid'),
    userName: string().required(),
    password: string().required(),
    confirmPassword: string().required().oneOf([ref('password'), ''], 'Passwords must match'),
    organizationCode: string().required(),
    OTP: string().required(),
    phoneForOTP: boolean().required(),
});