import rest_api from "@/redux/rest-api";


const organizationApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createOrganization: build.mutation<any, any>({
            query: (payload: any) => ({ method: "", url: "", data: "" })
        }),
        fetchOrganizationList: build.mutation({
            query: (params: { limit: number, offset: number }) => ({ method: "GET", url: "/organization", params })
        }),
        fetchOrganizationCodeList: build.mutation<any, any>({
            query: (params: { limit: number, offset: number }) => ({ method: "GET", url: "/organization/get/org-code", params })
        })
    })
})


export const {


    useCreateOrganizationMutation,
    useFetchOrganizationListMutation,
    useFetchOrganizationCodeListMutation

} = organizationApi;
export default organizationApi;