'use client'
import DataTable, { TableColumn } from "react-data-table-component";
import React, { useEffect } from "react";
import { DefaultPagination } from "@/components/table/default.pagination";
import { useOrganizationCodeHook } from "./hooks/useOrganizationCode.hook";
import { CubeTransparentIcon, EllipsisVerticalIcon, PaintBrushIcon, XMarkIcon } from "@heroicons/react/20/solid";

type DataRow = {
    id: number;
    code: string;
    organizationId?: string;
    asOrganization: boolean;
};

const columns = (): TableColumn<DataRow>[]=>[
    {
        name: 'ID',
        selector: row => row?.id,
    },
    {
        name: 'Organization',
        cell: row => <>{row.asOrganization ? "AS ORGANIZATION": `${row.organizationId}`}</>,
    },
    {
        name: 'Code',
        selector: row => row.code,
    },
    {
        name: 'Action',
        cell: (row, index, column, id) =>
            <>
                <button className="bg-lime-600 p-1"><CubeTransparentIcon title="Revoke" className="h-4 bg-lime-600" /></button>
                <button className="bg-pink-600 p-1"><XMarkIcon title="Remove" className="h-4 bg-lime-600" /></button>
                <button className="bg-teal-600 p-1"><PaintBrushIcon title="Rewrite" className="h-4 bg-lime-600" /></button>
            </>
        ,
    },
];

// const data = [
//     {
//         id: 1,
//         title: 'Beetlejuice',
//         year: '1988',
//     },
//     {
//         id: 18,
//         title: 'Ghostbusters',
//         year: '1984',
//     },
//     {
//         id: 3,
//         title: 'Beetlejuice',
//         year: '1988',
//     },
//     {
//         id: 4,
//         title: 'Ghostbusters',
//         year: '1984',
//     },
//     {
//         id: 5,
//         title: 'Beetlejuice',
//         year: '1988',
//     },
//     {
//         id: 6,
//         title: 'Ghostbusters',
//         year: '1984',
//     },
//     {
//         id: 7,
//         title: 'Beetlejuice',
//         year: '1988',
//     },
//     {
//         id: 8,
//         title: 'Ghostbusters',
//         year: '1984',
//     },
// ]

export default function OrganizationCodeList() {

    const { fetchOrganizationCodeList, data, offset, setOffset, limit, setLimit, isLoading, count } = useOrganizationCodeHook({})

    useEffect(() => {

    }, [])

    return (
        <>
            <DataTable
                title="Organization List Feature"
                columns={columns()}
                data={data?.data?.rows??[]}
                progressPending={isLoading}
                selectableRows
            />
            <DefaultPagination limit={limit} setOffset={setOffset} offset={offset} setLimit={setLimit} count={count} onPage={fetchOrganizationCodeList} />
        </>
    )
}