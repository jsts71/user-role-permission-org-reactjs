import { useCallback, useEffect, useState } from "react"
import { useFetchOrganizationListMutation } from "../redux/api"

export interface OrganizationHookProps {

}

export interface OrganizationListHookInterface {
    data: any
    limit: number
    offset: number
    count: number
    setLimit: (limit: number) => void
    setOffset: (offset: number) => void
    setCount: (count: number) => void
    fetchOrganizationList: (body: { limit: number, offset: number }) => any
    isLoading: boolean
}

export const useOrganizationListHook = (props: OrganizationHookProps): OrganizationListHookInterface => {

    const [limit, setLimit] = useState(10)
    const [offset, setOffset] = useState(0)
    const [count, setCount] = useState(0)

    const [fetchOrganizationList, { data, isError, isLoading, isSuccess, isUninitialized }] = useFetchOrganizationListMutation()

    console.log(data)
    useEffect(() => {
        if (isSuccess) {
            setCount(data.data.count)
        }
    }, [isSuccess])

    console.log(count, data?.data?.count)


    useEffect(() => {
        fetchOrganizationList({ limit, offset })
    }, [limit, offset])

    return {

        data,
        offset,
        setOffset,
        limit,
        setLimit,
        fetchOrganizationList,
        isLoading,
        count,
        setCount
    }
}