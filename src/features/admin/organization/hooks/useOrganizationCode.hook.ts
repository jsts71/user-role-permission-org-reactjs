import { useCallback, useEffect, useState } from "react"
import { useFetchOrganizationCodeListMutation } from "../redux/api"


export interface OrganizationCodeHookProps {

}

export interface OrganizationCodeHookInterface {
    data: any
    limit: number
    offset: number
    count: number
    setLimit: (limit: number) => void
    setOffset: (offset: number) => void
    setCount: (count: number) => void
    fetchOrganizationCodeList: (body: { limit: number, offset: number }) => any
    isLoading: boolean
}

export const useOrganizationCodeHook = (props: OrganizationCodeHookProps): OrganizationCodeHookInterface => {


    const [limit, setLimit] = useState(10)
    const [offset, setOffset] = useState(0)
    const [count, setCount] = useState(0)

    const [fetchOrganizationCodeList, { data, isError, isLoading, isSuccess, isUninitialized }] = useFetchOrganizationCodeListMutation()

    console.log(data)
    useEffect(() => {
        if (isSuccess) {
            setCount(data.data.count)
        }
    }, [isSuccess])

    console.log(count, data?.data?.count)


    useEffect(() => {
        fetchOrganizationCodeList({ limit, offset })
    }, [limit, offset])

    return {
        data,
        offset,
        setOffset,
        limit,
        setLimit,
        fetchOrganizationCodeList,
        isLoading,
        count,
        setCount
    }
}