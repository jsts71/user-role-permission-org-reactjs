interface HeaderInterface {
    text?: string
    size?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
    weight?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
    color?: string
    justify?: 'center' | 'end' | 'right'
}

export const DefaultHeader = (props: HeaderInterface) => {
    const { text='Header', size = 5, weight = 5, color = 'white', justify = 'center' } = props

    return (
        <>
            <div style={{ fontSize: `${size * 10}px`, fontWeight: `${100 * weight}`, color: `${color}` }} className={`flex justify-${justify}`}>
                {text}
            </div>

        </>
    )
}

export default DefaultHeader