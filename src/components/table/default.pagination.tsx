import { useCallback, useEffect, useState } from "react";
import { ChevronDoubleLeftIcon, ChevronDoubleRightIcon, ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/20/solid'
import { arrayRange } from "@/utils/utils";

export interface DefaultPaginationProps {
    offset: number
    limit: number
    count: number
    setLimit: (limit: number) => void
    setOffset: (offset: number) => void
    onPage: (body: { limit: number, offset: number }) => any
}

export const DefaultPagination = (props: any) => {

    const { limit, offset, setOffset, count, onPage, setLimit, isLoading } = props;
    const [page, setPage] = useState(1)
    const [range, setRange] = useState(arrayRange(count / limit))
    const [active, setActive] = useState(1);

    useEffect(() => {
        let newOffset = (page - 1) * limit
        setOffset(newOffset)
    }, [page])

    useEffect(() => {
        let newRange = arrayRange(count / limit)
        setRange(newRange.length ? newRange : [1])
    }, [count, limit])

    const changeLimit = (e: any) => {
        let newLimit = e.target.value;
        setLimit(newLimit)
        setPage(1)
    }

    return (
        <>
            <div className="flex items-center justify-between border-t border-gray-200 bg-white px-4 py-3 sm:px-6">
                <div className="flex flex-1 justify-between sm:hidden">
                    <a
                        href="#"
                        className="relative inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
                    >
                        Previous
                    </a>
                    <a
                        href="#"
                        className="relative ml-3 inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
                    >
                        Next
                    </a>
                </div>
                <div className="hidden sm:flex sm:flex-1 sm:items-center sm:justify-between">
                    <div>
                        <p className="text-sm text-gray-700">
                            Showing <span className="font-medium">{offset + 1}</span> to <span className="font-medium">{offset + limit > count ? parseInt(count) : parseInt(offset + limit)}</span> of{' '}
                            <span className="font-medium">{count}</span> results
                        </p>
                    </div>
                    <div>
                        <div className="mt-2 flex">
                            <div className="flex text-gray-700 py-1.5 px-2">
                                Limit:
                            </div>
                            <select
                                onChange={changeLimit}
                                value={limit}
                                id="limit"
                                name="limit"
                                autoComplete="limit"
                                className="flex block w-full px-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6"
                            >
                                <option>5</option>
                                <option>10</option>
                                <option>20</option>
                                <option>40</option>
                                <option>80</option>
                                <option>150</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <nav className="isolate inline-flex -space-x-px rounded-md shadow-sm" aria-label="Pagination">
                            <button
                                onClick={() => { setPage(1) }}
                                disabled={page == 1}
                                className="relative disabled:bg-gray-500 inline-flex items-center rounded-l-md px-2 py-2 text-gray-100 ring-0 ring-inset ring-lime-300 hover:bg-lime-800 focus:z-20 focus:outline-offset-0 bg-lime-700"
                            >
                                <span className="sr-only">Previous</span>
                                <ChevronDoubleLeftIcon className="h-5 w-5" aria-hidden="true" />
                            </button>
                            <button
                                onClick={() => { setPage(page - 1) }}
                                disabled={page == 1}
                                className="relative disabled:bg-gray-500 inline-flex items-center rounded-l-md px-2 py-2 text-gray-100 ring-0 ring-inset ring-lime-300 hover:bg-lime-500 focus:z-20 focus:outline-offset-0 bg-lime-400"
                            >
                                <span className="sr-only">Previous</span>
                                <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
                            </button>
                            {/* Current: "z-10 bg-indigo-600 text-white focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600", Default: "text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:outline-offset-0" */}

                            {
                                range.map((value, index) => (
                                    <button
                                        // hidden={index>page+3}
                                        key={index}
                                        aria-current="page"
                                        onClick={() => { setPage(value) }}
                                        className={`${page == value ? 'bg-amber-800 ' : 'bg-amber-500 '} ${index > (page + 2) ? 'hidden ' : " "} ${index < (page - 4) ? 'hidden ' : " "} display_none relative z-10 inline-flex items-center px-4 py-2 text-sm font-semibold text-white focus:z-20 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-amber-600`}
                                    >
                                        {value}
                                    </button>
                                )
                                )
                            }

                            <button
                                onClick={() => { setPage(page + 1) }}
                                disabled={range.length == page}
                                className="relative disabled:bg-gray-500 inline-flex items-center rounded-r-md px-2 py-2 text-gray-100 ring-0 ring-inset ring-lime-300 hover:bg-lime-500 focus:z-20 focus:outline-offset-0 bg-lime-400"
                            >
                                <span className="sr-only">Next</span>
                                <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
                            </button>
                            <button
                                onClick={() => { setPage(range.length) }}
                                disabled={range.length == page}
                                className="relative disabled:bg-gray-500 inline-flex items-center rounded-r-md px-2 py-2 text-gray-100 ring-0 ring-inset ring-lime-300 hover:bg-lime-800 focus:z-20 focus:outline-offset-0 bg-lime-700"
                            >
                                <span className="sr-only">Next</span>
                                <ChevronDoubleRightIcon className="h-5 w-5" aria-hidden="true" />
                            </button>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    )
}