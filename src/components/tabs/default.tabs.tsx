import { Dispatch, SetStateAction } from "react"

interface Tabs {
    view: any
    name: string
}

interface DefaultTabsProps {
    view: any,
    setView: Dispatch<SetStateAction<any>>
    tabs: Tabs[]
}

export default function DefaultTabs(props: DefaultTabsProps) {

    const { view, setView, tabs = [] } = props;

    return (
        <>
            <div className="flex flex-nowrap overflow-auto rounded bg-amber-500 no-scrollbar">
                {tabs.map((tab, index) => (
                    <div key={index} className="border-0 flex-auto rounded-md px-2 py-2 min-w-[15%]">
                        <button className="rounded-sm bg-amber-600 min-w-full px-3 py-2 hover:bg-amber-700 focus:bg-amber-800" onClick={() => { setView(tab.view) }}>{tab.name}</button>
                    </div>
                ))}
            </div>
        </>
    )
}