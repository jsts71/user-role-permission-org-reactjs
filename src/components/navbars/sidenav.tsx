import Link from "next/link";


export default function SideNav() {
    return (
        <>
            <div className="mb-2 p-4">
                <h5 className="block antialiased tracking-normal font-sans text-xl font-semibold leading-snug text-gray-900">Admin Panel</h5>
            </div>

            <nav className="flex flex-col gap-1 min-w-[240px] p-2 font-sans text-base font-normal text-gray-700">

                <Link href="/admin/organization">
                    <div role="button" tabIndex={0} className="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-gray-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
                        <div className="grid place-items-center mr-4">
                            <svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 64 64" enableBackground="new 0 0 64 64" fill="#000000">
                                <g id="SVGRepo_bgCarrier" strokeWidth="0"></g><g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <g> <rect x="18" y="25" fill="#506C7F" width="4" height="29"></rect> <rect x="30" y="25" fill="#506C7F" width="4" height="29"></rect> <rect x="42" y="25" fill="#506C7F" width="4" height="29"></rect> </g> <g> <rect x="48" y="25" fill="#B4CCB9" width="4" height="29"></rect> <rect x="24" y="25" fill="#B4CCB9" width="4" height="29"></rect> <rect x="36" y="25" fill="#B4CCB9" width="4" height="29"></rect> <rect x="12" y="25" fill="#B4CCB9" width="4" height="29"></rect> </g> <g> <path fill="#F9EBB2" d="M8,56c-1.104,0-2,0.896-2,2h52c0-1.104-0.895-2-2-2H8z"></path> <path fill="#F9EBB2" d="M60,60H4c-1.104,0-2,0.896-2,2h60C62,60.896,61.105,60,60,60z"></path> </g> <path fill="#F9EBB2" d="M4,23h56c0.893,0,1.684-0.601,1.926-1.461c0.24-0.86-0.125-1.785-0.889-2.248l-28-17 C32.725,2.1,32.365,2,32,2c-0.367,0-0.725,0.1-1.037,0.29L2.961,19.291c-0.764,0.463-1.129,1.388-0.888,2.247 C2.315,22.399,3.107,23,4,23z"></path> <g> <path fill="#394240" d="M60,58c0-2.209-1.791-4-4-4h-2V25h6c1.795,0,3.369-1.194,3.852-2.922c0.484-1.728-0.242-3.566-1.775-4.497 l-28-17C33.439,0.193,32.719,0,32,0s-1.438,0.193-2.076,0.581l-28,17c-1.533,0.931-2.26,2.77-1.775,4.497 C0.632,23.806,2.207,25,4,25h6v29H8c-2.209,0-4,1.791-4,4c-2.209,0-4,1.791-4,4v2h64v-2C64,59.791,62.209,58,60,58z M4,23 c-0.893,0-1.685-0.601-1.926-1.462c-0.241-0.859,0.124-1.784,0.888-2.247l28-17.001C31.275,2.1,31.635,2,32,2 c0.367,0,0.725,0.1,1.039,0.291l28,17c0.764,0.463,1.129,1.388,0.887,2.248C61.686,22.399,60.893,23,60,23H4z M52,25v29h-4V25H52z M46,25v29h-4V25H46z M40,25v29h-4V25H40z M34,25v29h-4V25H34z M28,25v29h-4V25H28z M22,25v29h-4V25H22z M16,25v29h-4V25H16z M8,56h48c1.105,0,2,0.896,2,2H6C6,56.896,6.896,56,8,56z M2,62c0-1.104,0.896-2,2-2h56c1.105,0,2,0.896,2,2H2z"></path> <path fill="#394240" d="M32,9c-2.762,0-5,2.238-5,5s2.238,5,5,5s5-2.238,5-5S34.762,9,32,9z M32,17c-1.656,0-3-1.343-3-3 s1.344-3,3-3c1.658,0,3,1.343,3,3S33.658,17,32,17z"></path> </g> <circle fill="#F76D57" cx="32" cy="14" r="3"></circle> </g> </g>
                            </svg>
                        </div>
                        Organization
                    </div>
                </Link>

                <Link href="/admin/permission">
                    <div role="button" tabIndex={0} className="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-blue-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
                        <div className="grid place-items-center mr-4">
                            <svg width="20px" height="20px" viewBox="0 0 91 91" enableBackground="new 0 0 91 91" id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="#000000"><g id="SVGRepo_bgCarrier" strokeWidth="0"></g><g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <path d="M62.93,60.806c-7.346,0-14.25-2.859-19.439-8.053c-10.713-10.717-10.713-28.156-0.004-38.877 C48.68,8.685,55.584,5.825,62.93,5.825s14.248,2.859,19.443,8.049c10.713,10.723,10.713,28.162,0,38.879 C77.18,57.946,70.275,60.806,62.93,60.806z M62.93,12.505c-5.563,0-10.787,2.164-14.717,6.094c-8.109,8.115-8.109,21.318,0,29.432 c3.93,3.932,9.154,6.096,14.717,6.096c5.561,0,10.787-2.164,14.719-6.098c8.109-8.111,8.109-21.314,0-29.43 C73.717,14.669,68.49,12.505,62.93,12.505z" fill="#647F94"></path> <path d="M71.971,24.276c-2.414-2.416-5.623-3.746-9.039-3.746c-3.412,0-6.623,1.33-9.043,3.744 c-4.982,4.988-4.982,13.1,0.002,18.088c2.418,2.412,5.627,3.742,9.041,3.742c3.416,0,6.629-1.332,9.047-3.75 C76.959,37.37,76.955,29.259,71.971,24.276z" fill="#6EC4A7"></path> <path d="M12.859,87.556L12.859,87.556c-1.107,0-2.17-0.439-2.953-1.223l-7.268-7.27 c-1.631-1.631-1.631-4.275,0-5.904c1.631-1.631,4.273-1.631,5.904,0l4.316,4.318l29.264-29.262 c1.627-1.627,4.273-1.629,5.904,0.002c1.631,1.629,1.631,4.275,0,5.906L15.811,86.335C15.027,87.118,13.967,87.556,12.859,87.556z" fill="#647F94"></path> <path d="M21.342,78.243c-0.854,0-1.709-0.326-2.359-0.979L5.285,63.577c-1.305-1.305-1.305-3.42-0.002-4.725 c1.305-1.305,3.42-1.305,4.723-0.002l13.697,13.691c1.305,1.303,1.305,3.418,0.002,4.723 C23.053,77.917,22.199,78.243,21.342,78.243z" fill="#647F94"></path> <path d="M29.359,70.228c-0.854,0-1.707-0.326-2.359-0.979l-7.279-7.268c-1.305-1.305-1.307-3.418-0.002-4.725 c1.305-1.303,3.42-1.307,4.725-0.002l7.277,7.27c1.305,1.303,1.307,3.418,0.004,4.725C31.072,69.901,30.217,70.228,29.359,70.228z" fill="#647F94"></path> </g> </g></svg>
                        </div>
                        Permission
                    </div>
                </Link>


                {/* <div role="button" tabIndex={0} className="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-blue-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
                    <div className="grid place-items-center mr-4">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true" className="h-5 w-5">
                            <path fillRule="evenodd" d="M6.912 3a3 3 0 00-2.868 2.118l-2.411 7.838a3 3 0 00-.133.882V18a3 3 0 003 3h15a3 3 0 003-3v-4.162c0-.299-.045-.596-.133-.882l-2.412-7.838A3 3 0 0017.088 3H6.912zm13.823 9.75l-2.213-7.191A1.5 1.5 0 0017.088 4.5H6.912a1.5 1.5 0 00-1.434 1.059L3.265 12.75H6.11a3 3 0 012.684 1.658l.256.513a1.5 1.5 0 001.342.829h3.218a1.5 1.5 0 001.342-.83l.256-.512a3 3 0 012.684-1.658h2.844z" clipRule="evenodd"></path>
                        </svg>
                    </div>
                    Example Pages
                    <div className="grid place-items-center ml-auto justify-self-end">

                        <div className="relative grid items-center font-sans font-bold uppercase whitespace-nowrap select-none bg-blue-500/20 text-blue-900 py-1 px-2 text-xs rounded-full" style={{ opacity: 1 }}>
                            <span className="">14</span>
                        </div>
                    </div>
                </div> */}


                <Link href="/admin/role">
                    <div role="button" tabIndex={0} className="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-blue-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
                        <div className="grid place-items-center mr-4">
                            <svg width="20px" height="20px" viewBox="0 -56 1136 1136" className="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="#000000">
                                <g id="SVGRepo_bgCarrier" strokeWidth="0"></g><g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"></g><g id="SVGRepo_iconCarrier"><path d="M1131.439228 1019.069479c34.07464-423.159488-245.654384-669.606306-554.703448-669.606306S-15.212044 596.702424 0.636626 1019.069479h565.005085c282.106325 0 338.369104 11.094069 565.797517 0z" fill="#48CFAD"></path><path d="M1073.591583 0q67.356847 79.24335-34.867074 588.778089l-237.730049-190.184039q136.298562-51.508177 272.597123-398.59405zM79.879976 0q-67.356847 79.24335 34.867074 588.778089L353.269533 396.216749Q216.178538 347.085872 79.879976 0zM517.303267 346.293439h118.865025v507.157439h-118.865025z" fill="#434A54"></path><path d="M634.583425 854.243311H517.303267V346.293439h18.225971a461.988729 461.988729 0 0 1 79.24335 0h18.22597z m-79.24335-39.621675h38.829242V383.537813H555.340075z" fill="#434A54"></path><path d="M13.315562 828.885439h1114.161499v39.621675H13.315562z" fill="#434A54"></path></g>
                            </svg>
                        </div>
                        Role
                    </div>
                </Link>

                <Link href="/admin/user">

                    <div role="button" tabIndex={0} className="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-blue-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
                        <div className="grid place-items-center mr-4">
                            <svg width="20px" height="20px" viewBox="0 0 1024 1024" className="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="#000000"><g id="SVGRepo_bgCarrier" strokeWidth="0"></g><g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"></g><g id="SVGRepo_iconCarrier"><path d="M691.573 338.89c-1.282 109.275-89.055 197.047-198.33 198.331-109.292 1.282-197.065-90.984-198.325-198.331-0.809-68.918-107.758-68.998-106.948 0 1.968 167.591 137.681 303.31 305.272 305.278C660.85 646.136 796.587 503.52 798.521 338.89c0.811-68.998-106.136-68.918-106.948 0z" fill="#4A5699"></path><path d="M294.918 325.158c1.283-109.272 89.051-197.047 198.325-198.33 109.292-1.283 197.068 90.983 198.33 198.33 0.812 68.919 107.759 68.998 106.948 0C796.555 157.567 660.839 21.842 493.243 19.88c-167.604-1.963-303.341 140.65-305.272 305.278-0.811 68.998 106.139 68.919 106.947 0z" fill="#C45FA0"></path><path d="M222.324 959.994c0.65-74.688 29.145-144.534 80.868-197.979 53.219-54.995 126.117-84.134 201.904-84.794 74.199-0.646 145.202 29.791 197.979 80.867 54.995 53.219 84.13 126.119 84.79 201.905 0.603 68.932 107.549 68.99 106.947 0-1.857-213.527-176.184-387.865-389.716-389.721-213.551-1.854-387.885 178.986-389.721 389.721-0.601 68.991 106.349 68.933 106.949 0.001z" fill="#E5594F"></path></g></svg>
                        </div>
                        User
                    </div>
                </Link>


                {/* <div role="button" tabIndex={0} className="flex items-center w-full p-3 rounded-lg text-start leading-tight transition-all hover:bg-blue-50 hover:bg-opacity-80 focus:bg-blue-50 focus:bg-opacity-80 active:bg-blue-50 active:bg-opacity-80 hover:text-blue-900 focus:text-blue-900 active:text-blue-900 outline-none">
                    <div className="grid place-items-center mr-4">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true" className="h-5 w-5">
                            <path fillRule="evenodd" d="M12 2.25a.75.75 0 01.75.75v9a.75.75 0 01-1.5 0V3a.75.75 0 01.75-.75zM6.166 5.106a.75.75 0 010 1.06 8.25 8.25 0 1011.668 0 .75.75 0 111.06-1.06c3.808 3.807 3.808 9.98 0 13.788-3.807 3.808-9.98 3.808-13.788 0-3.808-3.807-3.808-9.98 0-13.788a.75.75 0 011.06 0z" clipRule="evenodd"></path>
                        </svg>
                    </div>Log Out
                </div> */}

            </nav>
        </>
    )
}