
import { useEffect, useState } from 'react';

export interface NavigationHookProps {
}

export interface NavigationHookInterface {
    topNavigators: { name: string, href: string, needAuth: boolean }[],
}

export const useNavigationHook = (props: NavigationHookProps): NavigationHookInterface => {

    const { } = props

    useEffect(() => {

    }, [])

    const topNavigators = [
        { name: 'Dashboard', href: '/', needAuth: false },
        { name: 'Admin Panel', href: '/admin', needAuth: true }
    ]

    return {
        topNavigators
    }
}