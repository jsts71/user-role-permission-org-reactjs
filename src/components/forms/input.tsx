interface InputProps {
    name?: string
    type?: 'text' | 'password'
    justify?: 'center' | 'end' | 'right'
}

export const DefaultInput = (props: InputProps | any) => {

    const { type = 'text', justify = 'center' } = props

    return (
        <div style={{ margin: '10px 5px' }} className={`flex justify-${justify}`}>
            <input type={type}
                style={{
                    padding: '5px',
                    color:'black',
                    borderRadius: '5px',
                }}
                
                className="grow" />
        </div>
    )
}

export default DefaultInput