interface InputProps {
    children: React.ReactNode
    background?: 'white' | 'red' | 'green' | 'black'
    color?: 'white' | 'red' | 'green' | 'black'
    justify?: 'center' | 'end' | 'right'
    onClick?: any
}

export const DefaultButton = (props: InputProps) => {

    const { children, background = 'white', color = 'black', onClick } = props

    return (
        <button className="grow button" style={{
            margin: '10px 5px',
            padding: '5px 10px',
            borderRadius: '5px',
            background: background,
            color: color
        }}
            onClick={onClick}>
            {children}
        </button>
    )
}

export const FlexButton = (props: InputProps) => {

    const { children, justify = 'center', background = 'white', color = 'black', onClick } = props

    return (
        <div style={{ margin: '10px 5px' }} className={`flex justify-${justify}`}>
            <button className="grow button" style={{
                padding: '5px 10px',
                borderRadius: '5px',
                background: background,
                color: color
            }}
                onClick={onClick}>
                {children}
            </button>
        </div>
    )
}

export default DefaultButton